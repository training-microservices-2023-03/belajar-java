package registrasi;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class Peserta {
    private String nama;
    private String email;

    /*
    public String getNama() {
        return nama;
    }
    public void setNama(String n) {
        nama = n;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    */
    
}
