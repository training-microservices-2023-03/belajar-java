# Cara Menggunakan SDKMAN #

1. Instalasi SDKMAN

    ```
    curl -s "https://get.sdkman.io" | bash
    ```

2. Melihat versi Java SDK yang tersedia
  
    ```
    sdk list java
    ```

3. Instalasi Java SDK

    ```
    sdk install java 17.0.8-tem
    ```

    Restart terminal/WSL setelah instalasi

4. Test instalasi Java SDK

    ```
    java -version
    ```

5. Melihat versi Maven yang tersedia

    ```
    sdk list maven
    ```

6. Instalasi Maven

    ```
    sdk install maven 3.9.5
    ```

    Restart terminal/WSL setelah instalasi

7. Test instalasi Maven

    ```
    mvn -version
    ```

# Instalasi VS Code Extension

[![VS Code Extension](img/vscode-extension-java.png)](img/vscode-extension-java.png)

# Compile dan Run #

1. Kompilasi kode program java (file `*.java`) menjadi bytecode (file `*.class`)

    ```
    javac <Nama File>
    ```

    Contoh

    ```
    javac Halo.java
    ```

2. Menjalankan program java

    ```
    java NamaClass
    ```

    Contoh

    ```
    java Halo
    ```

# Classpath #

Classpath : tempat/lokasi yang akan dicari Java untuk menemukan class yang dibutuhkan

Cara setting classpath:

* Menggunakan command line option `-cp`

    ```
    java -cp <lokasi folder> NamaClass
    ```

    Contoh

    ```
    java -cp /Users/endymuhardin/tmp/belajar-java Halo
    ```

* Menggunakan environment variable

    Di Mac/Linux :

    ```
    export CLASSPATH=/Users/endymuhardin/tmp/belajar-java
    ```

    Di Windows

    ```
    set CLASSPATH=c:\Users\endymuhardin\tmp
    ```

    Setelah setting classpath di env variable, kita bisa menjalankan aplikasi seperti biasa

    ```
    java Halo
    ```

## Memisahkan folder source dan hasil kompilasi 

1. Pindahkan source code ke folder `src`

2. Buat folder `bin` untuk menampung hasil compile

3. Arahkan kompilasi ke folder `bin`

    ```
    cd src
    javac Halo.java -d ../bin
    ```

4. Cek folder `bin`. Harusnya ada file hasil kompilasi sesuai dengan struktur package

    [![Hasil kompilasi](img/hasil-kompilasi.png)](img/hasil-kompilasi.png)

# Membuat file jar #

1. Compress file hasil compile dengan menggunakan algoritma `zip`.

2. Rename file hasil zip, ganti akhirannya menjadi `jar`. Misalnya `aplikasi.zip` direname menjadi `aplikasi.jar`

3. Cek isi file `jar` tersebut, pastikan file hasil compile kita berada di top level, bukan di subfolder. Isi file jar bisa dilihat dengan perintah berikut

    ```
    jar -tvf namafile.jar
    ```

    Contoh

    ```
    jar -tvf aplikasi.jar
    ```

    Outputnya seperti ini

    ```
    1096 Sat Oct 21 11:20:58 WIB 2023 Halo.class
       0 Sat Oct 21 11:20:28 WIB 2023 registrasi/
     251 Sat Oct 21 11:20:58 WIB 2023 registrasi/Peserta.class
    ```

4. Aplikasi bisa dijalankan dengan mencantumkan file jar sebagai classpath

    ```
    java -cp aplikasi.jar Halo
    ```

# Menggunakan Maven #

1. Membuat jar

    ```
    mvn package
    ```

2. Membersihkan hasil compile

    ```
    mvn clean
    ```

3. Bersihkan dulu, kemudian buat jar

    ```
    mvn clean package
    ```